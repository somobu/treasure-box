#!/usr/bin/python

import os
import sqlite3

targ_uuid = '7892d084-f8b4-4242-a54e-ad5854a7015f'
dest_uuid = 'a7a0be15-d2ba-4167-8b66-ad6bcd30a5e6'

current_folder = os.path.abspath(os.path.dirname(__file__))
conn = sqlite3.connect(os.path.join(current_folder, '../app/treasure-box.db'))

para_count = conn.cursor().execute("""
  SELECT max(para_order) FROM article_text WHERE article = ?
""", [dest_uuid] ).fetchone()[0];


para_offset = para_count + 2

conn.cursor().execute("""
  UPDATE article_text
  SET article = ?, para_order = para_order + ?
  WHERE article = ?
""", [dest_uuid, para_offset, targ_uuid]);

conn.cursor().execute("""
  UPDATE comments
  SET article = ?, para = para + ?
  WHERE article = ?
""", [dest_uuid, para_offset, targ_uuid]);

conn.cursor().execute("""
  UPDATE reactions
  SET article = ?
  WHERE article = ?
""", [dest_uuid, targ_uuid]);

conn.cursor().execute("""
  UPDATE article_tags
  SET article = ?
  WHERE article = ?
""", [dest_uuid, targ_uuid]);


# Finally, delete source article_meta
conn.cursor().execute("DELETE FROM article_meta WHERE uuid = ?", [targ_uuid]);


conn.commit()
conn.close()

